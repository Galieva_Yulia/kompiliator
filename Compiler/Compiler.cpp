#include "Compiler.h"
#include <string>
#include <iostream>
using namespace std;

Compiler::Compiler(string code)
{
	lexer = new CLexer(code);
	syntaxer = new Syntaxer(lexer);
}

Compiler::~Compiler()
{
	delete lexer;
	lexer = nullptr;

	delete syntaxer;
	syntaxer = nullptr;
}

