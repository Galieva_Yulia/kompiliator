#include "CScope.h"



CScope::CScope(CScope * scope)
{
	ParentScope = scope;
}

CScope::CScope()
{
}


CScope::~CScope()
{
	for (auto i : Idents)
		delete i.second;
}

void CScope::addIdentifier(string key, CIdentifier* value)
{
	Idents.insert({ key,value });
}

void CScope::addIdentsList(initializer_list<std::pair<string, CIdentifier*>> list)
{
	std::initializer_list<std::pair<string, CIdentifier*>>::iterator it;  // same as: const int* it
	for (it = list.begin(); it != list.end(); ++it)
		Idents.insert(std::make_pair(it->first, it->second));
}

CIdentifier * CScope::getIdentifier(string key)
{
	if (Idents.count(key) > 0)
		return Idents[key];
	
	if (this->getParentScope() != nullptr)
		return this->getParentScope()->getIdentifier(key);
	else
		return nullptr;
}

vector<CIdentifier*> CScope::getIdents()
{
	vector<CIdentifier*> v;
	for (auto it : Idents) {
		v.insert(v.end(), it.second);
	}
	return v;
}

CScope * CScope::getParentScope()
{
	return ParentScope;
}
 