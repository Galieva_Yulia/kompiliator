#pragma once
#include "CIdentifier.h"
#include "CType.h"
class CVariable :
	public CIdentifier
{
public:
	CVariable();

	CVariable(UseType mTypeofUse, string mIdentName, CType *type)
		: CIdentifier(mTypeofUse, mIdentName)
	{
		this->type = type;
	}
	~CVariable();
	CType* getType();
private:
	CType * type;
};

