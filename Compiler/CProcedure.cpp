#include "CProcedure.h"



CProcedure::CProcedure()
{
}


CProcedure::~CProcedure()
{
}

void CProcedure::addParams(vector<CIdentifier*> v)
{
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i]->getTypeofUse() == UseType::VAR)
			mParams.insert(mParams.end(), dynamic_cast<CVariableProc*>(v[i]));
		if (v[i]->getTypeofUse() == UseType::PROC)
			mProcName = v[i]->getIdentName();
	}
}

CVariableProc * CProcedure::getParam(int index)
{
	if (index < mParams.size())
		return mParams[index];
	else return nullptr;
}

string CProcedure::getProcName()
{
	return mProcName;
}
