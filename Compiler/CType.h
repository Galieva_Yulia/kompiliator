#pragma once
#include "CIdentifier.h"
//#include "Enums.h"
class CType:
	public CIdentifier
{
public:

	CType(UseType mTypeofUse, string mIdentName, CType *baseType)
		: CIdentifier(mTypeofUse,mIdentName)
	{
		mBaseType = baseType;
	}

	CType(UseType mTypeofUse, string mIdentName)
		: CIdentifier(mTypeofUse, mIdentName)
	{
	}

	CType(CIdentifier* identifier, CType *baseType)		
	{
		mBaseType = baseType;
	}

	void addBaseType(CType* baseType);
	CType * getBaseType();
	CType();
	~CType();

private:
	CType * mBaseType;
};

