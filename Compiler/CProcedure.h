#pragma once
#include "CVariableProc.h"
#include <vector>

class CProcedure:
	public CIdentifier
{
public:
	CProcedure();

	CProcedure(UseType mTypeofUse, string mIdentName )
		: CIdentifier(mTypeofUse, mIdentName)
	{
		
	}

	~CProcedure();

	void addParams(vector<CIdentifier*> variable);
	CVariableProc* getParam(int index);
	string getProcName();
private:
	vector<CVariableProc*> mParams;
	string mProcName;
};

