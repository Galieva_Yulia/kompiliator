#include "Enums.h"
#include <iostream>
#include <string>
#include "Structs.h"
using namespace std;
class CValue //���������
{
public:
	ValueType type;
	virtual ~CValue()
	{

	}
};

class CIntValue :
	public CValue
{
public:
	CIntValue(int value)
	{
		this->value = value;
		this->type = ValueType::intc;
	}
	~CIntValue(){}

	int value;
};

class CRealValue :
	public CValue
{
public:
	CRealValue(float value)
	{
		this->value = value;
		this->type = ValueType::floatc;
	}
	~CRealValue(){}

	float value;
};

class CCharValue :
	public CValue
{
public:
	CCharValue(char value)
	{
		this->value = value;
		this->type = ValueType::charc;
	}
	~CCharValue() {}

	char value;
};

class CStringValue :
	public CValue
{
public:
	CStringValue(string value)
	{
		this->value = value;
		this->type = ValueType::stringc;
	}
	~CStringValue() {}

	string value;
};

//TODO: ���� ����������
class CBoolValue :
	public CValue
{
public:
	CBoolValue(bool value)
	{
		this->value = value;
		this->type = ValueType::booleanc;
	}
	~CBoolValue() {}

	bool value;
};


class CSymbol  //��� ������ (�������): 1)������������� 2)�������� ����� 3)��������� 4)��������
{
public:
	CSymbol();
	~CSymbol();

public:
	unsigned symbolCode; // ��� �������
	textPosition position;//������� ������� - ������ ������ �������
	SymbolType curSymbolType;

	union {					//������ ����� ���� ����������, ���������������, ���� ������ ��� ����������. ����� ��������� 1 �� 4
		operandType operatorVal;
		keywordType keywordVal;
		CValue *valueVal;
		string identVal = "";
	};
};


