#pragma once
#include "CVariable.h"
class CVariableProc:
	public CVariable
{
public:
	enum VarProcType
	{
		byRef = 0,
		byVal = 1
	};
	CVariableProc();
	CVariableProc(UseType mTypeofUse, string mIdentName, CType *type, int typeInd)
		: CVariable(mTypeofUse, mIdentName,type)
	{
		if (typeInd == 30)
			mType = VarProcType::byRef;
		else
			mType = VarProcType::byVal;
	}
	~CVariableProc();
private:
	VarProcType mType;
};

