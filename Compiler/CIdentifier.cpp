#include "CIdentifier.h"

CIdentifier::CIdentifier()
{
}

CIdentifier::CIdentifier(UseType mTypeofUse, string mIdentName)
{
	this->mIdentName = mIdentName;
	this->mTypeofUse = mTypeofUse;
}

CIdentifier::~CIdentifier()
{
}

int CIdentifier::getTypeofUse()
{
	return mTypeofUse;
}

string CIdentifier::getIdentName()
{
	return mIdentName;
}
