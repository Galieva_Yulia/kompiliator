#include <atlstr.h> 
#include <fstream>
#include <iostream>
#include <string>
#include "Syntaxer.h"

using namespace std;


class Compiler
{
public:
	Compiler(string code);
	~Compiler();
	
	CLexer *lexer;
	Syntaxer *syntaxer;

	void startAnalize()
	{
		syntaxer->scanProgram();
	}
};

