#include "Syntaxer.h"
#include <iostream>
#include <vector>


Syntaxer::Syntaxer(CLexer *lex)
{
	lexer = lex; //����� ��������� �� ���������� ����� � ������������ ������, ������ ����� ����� ����������� ������
	curSymbol = lexer->getNextSymbol(); //����� ������ ������ ����� ���������
	int expextedSym = -1;
	bool isSkipping = false;

	fictScope = new CScope();
	initializeFictScope();
}


Syntaxer::~Syntaxer()
{
	/*delete expextedSym;
	expextedSym = nullptr;*/

	delete curSymbol; //����� �������� ��� �������� ������������������ �������� ��������
	curSymbol = nullptr;
	delete fictScope;
	fictScope = nullptr;
	delete curScope;
	curScope = nullptr;
}

void Syntaxer::initializeFictScope()
{
	fictScope->addIdentsList({ 
		//types
		{"integer", new CType(CIdentifier::TYPE, "integer", nullptr)},
		{ "real", new CType(CIdentifier::TYPE, "real", nullptr)},
		{ "string", new CType(CIdentifier::TYPE, "string", nullptr)},
		{ "boolean", new CType(CIdentifier::TYPE, "boolean", nullptr)},
		{ "char", new CType(CIdentifier::TYPE, "char", nullptr)},

		//procedures
		{ "Read", new CProcedure(CIdentifier::PROC, "Read") },
		{ "Readln", new CProcedure(CIdentifier::PROC, "Readln") },
		{ "Write", new CProcedure(CIdentifier::PROC, "Write") },
		{ "Writeln", new CProcedure(CIdentifier::PROC, "Writeln") },
		{ "new", new CProcedure(CIdentifier::PROC, "new")},

		//vars

		});
}

bool Syntaxer::checkSymbol(int symCode)
{
	bool isExist = false;
	if (!isSkipping)
	{
		if (curSymbol->symbolCode == symCode) {
			curSymbol = lexer->getNextSymbol();
			isExist = true;
		}
		else
			syntaxError(symCode);
	}
	else {
		curSymbol = lexer->getNextSymbol();
		if (curSymbol != nullptr && curSymbol->symbolCode == expextedSym)
		{
			isSkipping = false;
			expextedSym = -1;
		}
	}
	return isExist;
}

void Syntaxer::syntaxError(int symCode)
{
	//cout << "SYNTAX ERROR! " << " line num:" << lexer->positionnow.lineNumber + 1 <<" Char num: "<< lexer->positionnow.charNumber << endl;
	//TODO ������� ����� � ���������� ���� ������
	error(symCode);
	isSkipping = true;
}

void Syntaxer::scanProgram()
{
	/*<���������>::=program <���>(<��� �����>{,<��� �����>});<����>.*/
	/*<program> ::= program <identifier> ; <block>.*/

	if (checkSymbol(keywordType::programsy))
	{
		curScope = new CScope(fictScope);
		curScope->addIdentifier(curSymbol->identVal, new CIdentifier(CIdentifier::PROG, curSymbol->identVal));
	}

	checkSymbol(keywordType::ident);		
 	checkSymbol(operandType::semicolon);
	scanBlock();
	checkSymbol(operandType::point);
}

void Syntaxer::scanBlock()
{
	/*<����>::=<������ �����><������ ����������><������ �������� � �������>
<������ ����������>*/
	scanTypesSection();				// <������ �����>
	if (curSymbol->symbolCode == keywordType::varsy)
		scanVariablesSection();			// <������ �������� ����������>
	scanFuncAndProcSections();	// <������ �������� � �������>
	scanOperatorsSection();			// <������ ����������>
}


void Syntaxer::scanTypesSection()
{
	//<������ �����>::=<�����>|type <����������� ����>;{<����������� ����>; }
	if (curSymbol->symbolCode == keywordType::typesy) {
		curSymbol = lexer->getNextSymbol();

		scanTypeDefine();
		checkSymbol(operandType::semicolon);

		while (curSymbol->symbolCode != keywordType::varsy) //����� ������� �������� ����� ���� ������ �������� ����������, ������� ���������� � var
		{
			scanTypeDefine();
			checkSymbol(operandType::semicolon);
		}
	}
}

void Syntaxer::scanTypeDefine()
{
	/*<����������� ����>::=<���>=<���>*/
	string curT;
	CType * type = nullptr;
	if (curSymbol->symbolCode == keywordType::ident)
	{
		curT = curSymbol->identVal;
		type = new CType(CIdentifier::TYPE, curSymbol->identVal);
	}
	
	checkSymbol(keywordType::ident);		// <���>
	checkSymbol(operandType::equal);		// =
	if (curSymbol->symbolCode == keywordType::ident)
	{
		type->addBaseType(dynamic_cast<CType*>(getIdentifier(fictScope,curSymbol->identVal)));
		curScope->addIdentifier(curT, type);
	}
 	scanType();
}

void Syntaxer::scanType()
{
	/*<���>::=<������� ���>|<��������� ���>|<��������� ���>  � ���� ����� ������ �������*/
	/*<������� ���>::=<������������ ���>|<������������ ���>|<��� ����>  �� ���� ������������ ���*/
	/*<��� ����>::=<���>*/
	checkSymbol(keywordType::ident);
}

void Syntaxer::scanVariablesSection()
{
	/*<������ ����������>::= var <�������� ���������� ����������>;{<�������� ���������� ����������>;}|<�����>*/
	checkSymbol(keywordType::varsy);

	do {
		scanOneTypeVariables(keywordType::varsy);
		checkSymbol(operandType::semicolon);
	}
	while (curSymbol->symbolCode == keywordType::ident);
}

void Syntaxer::scanOneTypeVariables(int code)
{
	/*<�������� ���������� ����������>::=<���>{,<���>}:<���>*/

	vector<string> varsNameVec;
	varsNameVec.reserve(5);

	if (curSymbol->symbolCode == keywordType::ident)
		varsNameVec.insert(varsNameVec.end(), curSymbol->identVal); //���������� ��� ����� ���������� ������ ����

	checkSymbol(keywordType::ident);					// <���>
	while (curSymbol->symbolCode == operandType::comma)
	{
		curSymbol = lexer->getNextSymbol();
		if (curSymbol->symbolCode == keywordType::ident)
			varsNameVec.insert(varsNameVec.end(), curSymbol->identVal);
		checkSymbol(keywordType::ident);				// <���>
	}
 	checkSymbol(operandType::colon);					// :

	if (curSymbol->symbolCode == keywordType::ident)
	{
		CIdentifier * id = getIdentifier(curScope,curSymbol->identVal);

		if (id != nullptr)
		{
			for (int i = 0; i < varsNameVec.size(); i++)
				curScope->addIdentifier(varsNameVec[i], new CVariableProc(CIdentifier::VAR, varsNameVec[i], dynamic_cast<CType*>(id),code));
		}
	}
	scanType();
}

void Syntaxer::scanFuncAndProcSections()
{
	/*<������ �������� � �������>::={<�������� ��������� ��� �������>;}*/
	while (curSymbol->symbolCode == keywordType::proceduresy) {
		scanProcedures();
		checkSymbol(operandType::semicolon);
	}
}

void Syntaxer::scanProcedures()
{
	/*<�������� ���������>::=<��������� ���������><����>*/
	scanProceduresTitle();
	scanBlock();

	CProcedure* proc = new CProcedure();
	proc->addParams(curScope->getIdents());
	closeCurrentScope();
	curScope->addIdentifier(proc->getProcName(), proc);
}

void Syntaxer::scanProceduresTitle()
{
	/*<��������� ���������>:: = procedure <���>; | procedure <���>
		(<������ ���������� ����������>{; <������ ���������� ����������>});*/

	if (curSymbol->symbolCode == keywordType::proceduresy)
		curScope = new CScope(curScope);

	checkSymbol(keywordType::proceduresy);

	if (curSymbol->symbolCode == keywordType::ident)
		curScope->addIdentifier(curSymbol->identVal, new CIdentifier(CIdentifier::PROC, curSymbol->identVal));

	checkSymbol(keywordType::ident);		// <���>

	if (curSymbol->symbolCode == operandType::semicolon)
		checkSymbol(operandType::semicolon);
	else
	{
		checkSymbol(operandType::leftpar);
		do
		{
			if (curSymbol->symbolCode == operandType::semicolon)
				checkSymbol(operandType::semicolon);
			else
				scanFormalParametersSection();
		} while (curSymbol->symbolCode == keywordType::varsy ||
			curSymbol->symbolCode == keywordType::functionsy ||
			curSymbol->symbolCode == keywordType::proceduresy ||
			curSymbol->symbolCode == keywordType::ident ||
			curSymbol->symbolCode == operandType::semicolon);

		checkSymbol(operandType::rightpar);
		checkSymbol(operandType::semicolon);

	}

}

void Syntaxer::scanFormalParametersSection()
{
	/*<������ ���������� ����������>::=<������ ����������>|var <������ ����������>|
	function <������ ����������>|procedure <���>{,<���>}*/

	switch (curSymbol->symbolCode)
	{
		case (keywordType::varsy):
			checkSymbol(keywordType::varsy);
			scanParametresGroup(keywordType::varsy);
			break;
		case (keywordType::ident):
			scanParametresGroup(keywordType::ident);
			break;
	}
}

void Syntaxer::scanParametresGroup(int code)
{
	/*<������ ����������>::=<���>{,<���>}:<��� ����>*/
	/*identIdent();
	checkSymbol(operandType::colon);					// :
	scanType();*/

	scanOneTypeVariables(code);
}

void Syntaxer::identIdent()
{
	/*<���>{,<���>}*/
	//curScope->addIdentifier(curSymbol->identVal, new CVariable(CIdentifier::PROG, curSymbol->identVal));


	checkSymbol(keywordType::ident);					// <���>
	while (curSymbol->symbolCode == operandType::comma)
	{
		curSymbol = lexer->getNextSymbol();
		checkSymbol(keywordType::ident);				// <���>
	}
}

void Syntaxer::scanOperatorsSection()
{
	/*<������ ����������>::=<��������� ��������>*/
	scanCompositeOperator();
}

void Syntaxer::scanCompositeOperator()
{
	/*<��������� ��������>::= begin <��������>{;<��������>} end;*/

	checkSymbol(keywordType::beginsy);
	scanOperator();
	while (curSymbol->symbolCode == operandType::semicolon)
	{
		checkSymbol(operandType::semicolon);
		scanOperator();
	}
	checkSymbol(keywordType::endsy);
/*	if (lexer->positionnow.lineNumber != lexer->lines.size() - 1)
		checkSymbol(operandType::semicolon);*/
	//else 
	//	checkSymbol(operandType::semicolon);
}

void Syntaxer::scanOperator()
{
	/*<��������>::=<������������ ��������>|<�����><������������ ��������>  � ��� �����*/
	/*<������������ ��������>::=<������� ��������>|<������� ��������>*/
	if (curSymbol->symbolCode == keywordType::ident)
		scanSimpleOperator();
	else
			if (curSymbol->symbolCode == keywordType::beginsy || curSymbol->symbolCode == keywordType::whilesy ||
				curSymbol->symbolCode == keywordType::repeatsy || curSymbol->symbolCode == keywordType::forsy ||
				curSymbol->symbolCode == keywordType::ifsy)
					scanHardOperator();

}

void Syntaxer::scanSimpleOperator()
{
	/*<������� ��������>::=<�������� ������������>|<�������� ���������>*/
	CType * leftType = nullptr; CType *rightType = nullptr;
	string name = curSymbol->identVal;

	checkSymbol(keywordType::ident); //��� ��������� ���������� � ident
	switch (curSymbol->symbolCode)
	{
		case (operandType::assign):
		{
			CVariable * variable = dynamic_cast<CVariable*>(getIdentifier(curScope,name));
			if (variable != nullptr)
				leftType = variable->getType();
			rightType = scanAssignmentOperator();
			if (!compareTypes(leftType, rightType))
				lexer->error(182, lexer->positionnow, ErrorType::sem); //���� �� ���������
		}
			break;

		case (operandType::leftpar):
		{
			//CProcedure * proc = dynamic_cast<CProcedure*>(curScope->getIdentifier(name));
			scanProcedureOperator(name);
		}
			break;
		default:
			syntaxError(1);
			break;
	}
	
}

CType* Syntaxer::scanAssignmentOperator()
{
	/*<�������� ������������>::=<����������>:=<���������>*/
	//scanVariable();
	checkSymbol(operandType::assign);
	return scanExpression();
}

CType* Syntaxer::scanVariable()
{
	/*	<����������>::=<������ ����������>
		<������ ����������>::=<��� ����������>
		<��� ����������>::=<���>*/
	CVariable* var = dynamic_cast<CVariable*>(getIdentifier(curScope,curSymbol->identVal));
	checkSymbol(keywordType::ident);
	if (var != nullptr)
		return var->getType();
	else return nullptr;
}

CType* Syntaxer::scanExpression()
{
	/*<���������>::=<������� ���������>|<������� ���������><�������� ���������><������� ���������>*/
	CType * typeOneExpr = nullptr;
	CType * typeTwoExpr = nullptr;
	typeOneExpr = scanSimpleExpression();
	if (curSymbol->symbolCode == operandType::equal || curSymbol->symbolCode == operandType::latergreater ||
		curSymbol->symbolCode == operandType::later || curSymbol->symbolCode == operandType::greater ||
		curSymbol->symbolCode == operandType::laterequal || curSymbol->symbolCode == operandType::greaterequal)
	{
		int operation = curSymbol->symbolCode;
		relationOperation();
		typeTwoExpr = scanSimpleExpression();

		//�������� ������������� ����� ������� ���������
		typeOneExpr = compareTypes(typeOneExpr, typeTwoExpr, operation);
	}
	return typeOneExpr;
}

CType* Syntaxer::scanSimpleExpression()
{
	/*<������� ���������>::=<����><���������>{<���������� ��������><���������>}*/
	CType * typeOneExpr = nullptr;
	CType * typeTwoExpr = nullptr;
	scanSign();
	typeOneExpr =  scanTerm();
	while (curSymbol->symbolCode == operandType::plus || curSymbol->symbolCode == operandType::minus)
	{
		int operation = curSymbol->symbolCode;
		scanSign();				//�� ����� or, ������� �������� ������ +-
		typeTwoExpr = scanTerm();

		//�������� ������������� ���� ���������
		typeOneExpr = compareTypes(typeOneExpr, typeTwoExpr, operation);
	}
	return typeOneExpr;
}

void Syntaxer::scanSign()
{
	if (curSymbol->symbolCode == operandType::plus)
		checkSymbol(operandType::plus);
	else
		if (curSymbol->symbolCode == operandType::minus)
			checkSymbol(operandType::minus);
}

CType* Syntaxer::scanTerm()
{
	/*<���������>::=<���������>{<����������������� ��������><���������>}*/
	CType * typeOneTerm= nullptr;
	CType * typeTwoTerm = nullptr;

	typeOneTerm = scanFactor();

	while (curSymbol->symbolCode == operandType::star || curSymbol->symbolCode == operandType::slash ||
		curSymbol->symbolCode == keywordType::divsy || curSymbol->symbolCode == keywordType::modsy ||
		curSymbol->symbolCode == keywordType::andsy)
	{
		int operation = curSymbol->symbolCode;
		multOpp();
		typeTwoTerm = scanFactor();
		typeOneTerm = compareTypes(typeOneTerm, typeTwoTerm, operation);

	}
	return typeOneTerm;
}

CType* Syntaxer::scanFactor()
{
	/*<���������>::=<����������>|<��������� ��� �����>|(<���������>)*/
	CType * type = nullptr;
	switch (curSymbol->curSymbolType)
	{
	case (SymbolType::stConst):
		type = unsignedConstant();
		break;
	case (SymbolType::stIdent):
		type = scanVariable();
		break;
	case (SymbolType::stOperator):
		if (curSymbol->symbolCode == operandType::leftpar)
		{
			checkSymbol(operandType::leftpar);
			type = scanExpression();
			checkSymbol(operandType::rightpar);
		}
		break;
	}

	return type;
	
}

CType* Syntaxer::unsignedConstant()
{
	/*<��������� ��� �����>::=<����� ��� �����>|<������>|<��� ���������>|nil
	<����� ��� �����>::=<����� ��� �����>|<������������ ��� �����>
	<������>::='<������>{<������>}'
	������ - any-character-except-quote | "''"
	<��� ���������>::=<���>
	*/

	CType * type = nullptr;
	if (curSymbol->symbolCode != keywordType::ident)
	{
		switch (curSymbol->symbolCode)
		{
		case (ValueType::intc):				
			checkSymbol(ValueType::intc);
			type = dynamic_cast<CType*>(fictScope->getIdentifier("integer"));
			break;
		case(ValueType::floatc):				
			checkSymbol(ValueType::floatc);
			type = dynamic_cast<CType*>(fictScope->getIdentifier("real"));
			break;
		case(ValueType::stringc):
			checkSymbol(ValueType::stringc);
			type = dynamic_cast<CType*>(fictScope->getIdentifier("string"));
			break;
		case(ValueType::charc):
			checkSymbol(ValueType::charc);
			type = dynamic_cast<CType*>(fictScope->getIdentifier("char"));
			break;
		case(ValueType::booleanc):
			checkSymbol(ValueType::charc);
			type = dynamic_cast<CType*>(fictScope->getIdentifier("boolean"));
			break;
		default:
			syntaxError(1); //���� 1
			break;
		}
	}
	else
	{
		checkSymbol(keywordType::ident);
		//TODO return GetIdentType(Symbol)
	}

	return type;
}

void Syntaxer::multOpp()
{
	/*<����������������� ��������>::=*|/|div|mod|and*/
	switch (curSymbol->symbolCode)
	{
	case (operandType::star):
		checkSymbol(operandType::star);
		break;
	case(operandType::slash):
		checkSymbol(operandType::slash);
		break;
	case(keywordType::divsy):
		checkSymbol(keywordType::divsy);
		break;
	case(keywordType::modsy):
		checkSymbol(keywordType::modsy);
		break;
	case(keywordType::andsy):
		checkSymbol(keywordType::andsy);
		break;
	default:
		syntaxError(1);
		break;
	}
}



void Syntaxer::scanProcedureOperator(string name)
{
	/*<�������� ���������>::=<��� ���������>|
	<��� ���������>(<����������� ��������>{,<����������� ��������>})*/

	if (curSymbol->symbolCode == operandType::leftpar)
	{
		CType * typeOne = nullptr;
		CType * typeTwo = nullptr;
		int i = 0;

		CProcedure * proc = dynamic_cast<CProcedure*>(curScope->getIdentifier(name));
		if (proc == nullptr)
			return;
		typeOne = proc->getParam(i)->getType(); i++;

		checkSymbol(operandType::leftpar);
		typeTwo = scanActualParameter(); 

		if (!compareTypes(typeOne , typeTwo))
			lexer->error(199, lexer->positionnow, ErrorType::sem); //������������� ���� ������������ ���������� ��������� ���������

		while (curSymbol->symbolCode == operandType::comma)
		{
			checkSymbol(operandType::comma);
			if (proc->getParam(i) != nullptr) {
				typeOne = proc->getParam(i)->getType(); i++;
			}
			typeTwo = scanActualParameter(); 
			if (!compareTypes(typeOne, typeTwo))
				lexer->error(199, lexer->positionnow, ErrorType::sem); //������������� ���� ������������ ���������� ��������� ���������
		}
		checkSymbol(operandType::rightpar);
	}
}

CType* Syntaxer::scanActualParameter()
{
	/*<����������� ��������>::=<���������>|<����������>|<��� ���������>*/
	CType * type = nullptr;
	switch (curSymbol->curSymbolType)
	{
	case (SymbolType::stConst):
		type = scanExpression();
		break;
	case (SymbolType::stOperator):
		if (curSymbol->symbolCode == operandType::plus || curSymbol->symbolCode == operandType::minus)
			type = scanExpression();
		break;
	case (SymbolType::stIdent):
		//?? ��� ��� ������ ����� ������ ��������� ��� ��� ���������� 
		//checkSymbol(SymbolType::stIdent);
		/*if (curSymbol->symbolCode == operandType::star || curSymbol->symbolCode == operandType::slash ||
			curSymbol->symbolCode == keywordType::divsy || curSymbol->symbolCode == keywordType::modsy ||
			curSymbol->symbolCode == keywordType::andsy || curSymbol->symbolCode == operandType::plus ||
			curSymbol->symbolCode == operandType::minus)
		{

		}*/
		type = scanExpression();
		break;

	}
	return type;
}

void Syntaxer::scanZnak()
{
	if (curSymbol->symbolCode == operandType::plus)
		checkSymbol(operandType::plus);
	else if (curSymbol->symbolCode == operandType::minus)
		checkSymbol(operandType::minus);

}

void Syntaxer::scanHardOperator()
{
	/*<������� ��������>::=<��������� ��������>|<�������� �����>| �������� �������� */
	if (curSymbol->symbolCode == keywordType::beginsy)
		scanHardOperator();
	else if (curSymbol->symbolCode == keywordType::whilesy ||
		curSymbol->symbolCode == keywordType::forsy ||
		curSymbol->symbolCode == keywordType::repeatsy)
		scanCycleOperator();
	else if (curSymbol->symbolCode == keywordType::ifsy)
		scanCondOperator();
}

void Syntaxer::scanCycleOperator()
{
	/*<�������� �����>::=<���� � ������������>|<���� � ������������>|<���� � ����������>*/
	switch (curSymbol->symbolCode)
	{
	case (keywordType::repeatsy):
		scanPostCycleOperator();
		break;
	case (keywordType::whilesy):
		scanPreCycleOperator();
		break;
	case (keywordType::forsy):
		scanParamCycleOperator();
		break;
	default:
		syntaxError(1);
		break;
	}
}

void Syntaxer::scanPostCycleOperator()
{
	/*<���� � ������������>::= repeat <��������> {;<��������>} until <���������>*/
	checkSymbol(keywordType::repeatsy);
	scanOperator();

	while (curSymbol->symbolCode == operandType::semicolon)
	{
		checkSymbol(operandType::semicolon);
		scanOperator();

	}
	checkSymbol(keywordType::untilsy);

	CType * type = nullptr;
	type = scanExpression();
	if (!compareTypes(type, new CType(CIdentifier::TYPE, "boolean")))
		lexer->error(135, lexer->positionnow, ErrorType::sem); //��� �������� ������ ���� boolean
}

void Syntaxer::scanPreCycleOperator()
{
	/*<���� � ������������>::= while <���������> do <��������>*/
	CType * type = nullptr;
	checkSymbol(keywordType::whilesy);
	type = scanExpression();
	if (!compareTypes(type, new CType(CIdentifier::TYPE, "boolean")))
		lexer->error(135, lexer->positionnow, ErrorType::sem); //��� �������� ������ ���� boolean
	
	checkSymbol(keywordType::dosy);
	scanOperator();
}

void Syntaxer::scanParamCycleOperator()
{
	/*<���� � ����������>::= for <�������� �����>:=<���������>
<�����������><���������> do <��������>*/

	CType * typeOne = nullptr;
	CType * typeTwo = nullptr;
	checkSymbol(keywordType::forsy);

	CVariable * variable = dynamic_cast<CVariable*>(getIdentifier(curScope, curSymbol->identVal));
	if (variable != nullptr) {
		typeOne = variable->getType();

		if (typeOne != nullptr)
		{
			if (!checkType(typeOne, 1, "integer"))
				lexer->error(143, lexer->positionnow, ErrorType::sem);
		}
	}


	checkSymbol(keywordType::ident);
	checkSymbol(operandType::assign);

	typeTwo = scanExpression();
	if (!compareTypes(typeOne, typeTwo))
		lexer->error(145, lexer->positionnow, ErrorType::sem); //�������� �����
	direction();
	typeTwo = scanExpression();
	if (!compareTypes(typeOne, typeTwo))
		lexer->error(145, lexer->positionnow, ErrorType::sem); //�������� �����
	checkSymbol(keywordType::dosy);
	scanOperator();

}

CIdentifier * Syntaxer::getIdentifier(CScope* scope, string ident)
{
	if (scope->getIdentifier(ident) == nullptr)
	{
		lexer->error(0, lexer->positionnow, ErrorType::sem);
		return nullptr;
	}
	return scope->getIdentifier(ident);
}

void Syntaxer::scanCondOperator()
{
	/*<�������� ��������>::= if <���������> then <��������>|if <���������> then <��������> else <��������>*/
	CType * type = nullptr;
	checkSymbol(keywordType::ifsy);
	type = scanExpression();
	if (!compareTypes(type, new CType(CIdentifier::TYPE, "boolean")))
		lexer->error(135, lexer->positionnow, ErrorType::sem); //��� �������� ������ ���� boolean

	checkSymbol(keywordType::thensy);
	scanOperator();

	if (curSymbol->symbolCode == keywordType::elsesy) {
		checkSymbol(keywordType::elsesy);
		scanOperator();
	}
}

void Syntaxer::relationOperation()
{
	/*(curSymbol->symbolCode == operandType::equal || curSymbol->symbolCode == operandType::latergreater ||
		curSymbol->symbolCode == operandType::later || curSymbol->symbolCode == operandType::greater ||
		curSymbol->symbolCode == operandType::laterequal || curSymbol->symbolCode == operandType::greaterequal)
	{*/
	switch (curSymbol->symbolCode)
	{
	case (operandType::equal):
		checkSymbol(operandType::equal);
		break;
	case (operandType::latergreater):
		checkSymbol(operandType::latergreater);
		break;
	case (operandType::later):
		checkSymbol(operandType::later);
		break;
	case (operandType::greater):
		checkSymbol(operandType::greater);
		break;
	case (operandType::laterequal):
		checkSymbol(operandType::laterequal);
		break;
	case (operandType::greaterequal):
		checkSymbol(operandType::greaterequal);
		break;
	default:
		syntaxError(1); 
		break;
	}
}

void Syntaxer::direction()
{
	if (curSymbol->symbolCode == keywordType::tosy)
		checkSymbol(keywordType::tosy);
	else
		if (curSymbol->symbolCode == keywordType::downtosy)
			checkSymbol(keywordType::downtosy);

}

void Syntaxer::error(int symCode)
{
	lexer->error(symCode, lexer->positionnow, ErrorType::synt);		// ������ ���� ���	
	skip(operandType::semicolon);
	expextedSym = operandType::semicolon;
}

void Syntaxer::skip(int symCode)
{
	lexer->skip(symCode);
}

void Syntaxer::closeCurrentScope()
{
	/*if (curScope == nullptr)
		return;*/

	CScope * prevScope = curScope;
	curScope = curScope->getParentScope();

	delete prevScope;
	prevScope = nullptr;
}

CType* Syntaxer::compareTypes(CType * typeOne, CType * typeTwo, int operation)
{
	if (typeOne == nullptr || typeTwo == nullptr)
		return nullptr;
	switch (operation)
	{
	case (operandType::star):
	{
		if (!((checkType(typeOne, 1, "integer") || checkType(typeOne, 1, "real")) &&
			(checkType(typeTwo, 1, "integer") || checkType(typeTwo, 1, "real"))))
		{
			lexer->error(213, lexer->positionnow, ErrorType::sem);
			typeOne = nullptr;
		}
	}
	break;
	case(operandType::slash):
		if (!((checkType(typeOne, 1, "integer") || checkType(typeOne, 1, "real")) &&
			(checkType(typeTwo, 1, "integer") || checkType(typeTwo, 1, "real"))))
		{
			lexer->error(214, lexer->positionnow, ErrorType::sem);
			typeOne = nullptr;
		}
		break;
	case(keywordType::divsy):
	case(keywordType::modsy):
		if (!(checkType(typeOne,1,"integer")|| checkType(typeTwo, 1, "integer")))
		{
			lexer->error(212, lexer->positionnow, ErrorType::sem);
			typeOne = nullptr;
		}
		break;
	case(keywordType::andsy):
		if (!(checkType(typeOne, 1, "boolean") || checkType(typeTwo, 1, "boolean")))
		{
			lexer->error(210, lexer->positionnow, ErrorType::sem);
			typeOne = nullptr;
		}
		break;
	case(operandType::plus):
	case(operandType::minus):
		if (!((checkType(typeOne, 1, "integer") || checkType(typeOne, 1, "real")) &&
			(checkType(typeTwo, 1, "integer") || checkType(typeTwo, 1, "real"))))
		{
			lexer->error(211, lexer->positionnow, ErrorType::sem);
			typeOne = nullptr;
		}
		break;
	case(operandType::later):
	case(operandType::greater):
	case(operandType::laterequal):
	case(operandType::greaterequal):
	case(operandType::latergreater):
	case(operandType::equal):
		if (!(typeOne->getIdentName() == typeTwo->getIdentName()))
		{
			if ((checkType(typeOne,1,"integer") && checkType(typeTwo, 1, "real")) ||
				(checkType(typeOne, 1, "real") && checkType(typeTwo, 1, "integer")) ||
				(checkType(typeOne, 1, "string") && checkType(typeTwo, 1, "char")) ||
				(checkType(typeOne, 1, "char") && checkType(typeTwo, 1, "string")))
					typeOne = new CType(CIdentifier::TYPE, "boolean");
			else
					typeOne = nullptr;
		}
		else typeOne = new CType(CIdentifier::TYPE, "boolean");
		break;
	}

	if (typeOne != nullptr && typeOne->getIdentName() != "boolean")
	{
		if (checkType(typeOne, 1, "integer") && checkType(typeTwo, 1, "real"))
			typeOne = typeTwo;
		else
			if (checkType(typeOne, 1, "string") && checkType(typeTwo, 1, "char"))
				typeOne = typeTwo;
	}
	return typeOne;
}

bool Syntaxer::compareTypes(CType* a, CType* b) {
	/*if (a.getTypeofUse != 1 || b.getTypeofUse != 1)
	return false;*/
	if (a == nullptr || b == nullptr)
		return false;
	if (a->getIdentName() == b->getIdentName())
		return true;
	if ((a->getIdentName() == "real" && b->getIdentName() == "integer") ||
		(a->getIdentName() == "string" && b->getIdentName() == "char"))
		return true;
	return false;
}

bool Syntaxer::checkType(CType * type, int use, string identName)
{
	bool val = type->getTypeofUse() == 1 && type->getIdentName() == identName;
	if (!val && type->getBaseType() != nullptr)
		val = checkType(type->getBaseType(), use, identName);
	return val;
}

