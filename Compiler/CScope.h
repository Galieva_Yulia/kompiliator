#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>
#include "CIdentifier.h"
#include "CType.h"
#include "CVariable.h"
#include "CVariableProc.h"
#include "CProcedure.h"
using namespace std;

class CScope
{
public:
	CScope(CScope * parentScope);
	CScope();
	~CScope();

	void addIdentifier(string key, CIdentifier *value);
	void addIdentsList(initializer_list<std::pair<string, CIdentifier*>> list);

	CIdentifier * getIdentifier(string key);
	vector<CIdentifier*> getIdents();
		
	CScope * getParentScope();
private:
	std::unordered_map<std::string, CIdentifier*> Idents;
	CScope *ParentScope;


};

