#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <Windows.h>
#include "Compiler.h"
using namespace std;

#include <experimental/filesystem> // C++-standard header file name  
#include <filesystem> // Microsoft-specific implementation header file name  
namespace fs = std::experimental::filesystem::v1;

int main()
{
	setlocale(LC_ALL, "Russian");
	string str;
	ifstream InputFile;

	ofstream fout;
	fout.open("output.txt");

	string input = "C:\\Users\\Admin\\Desktop\\tests\\errors";

	if (fs::is_directory(input))
	{
		for (auto & p : fs::directory_iterator(input)) {
			if (fs::is_directory(p))
				continue;

			ifstream fin(p);
			std::cout << p << endl;

			fout << p << endl;
			string str;
			getline(fin, str, '\0');

			Compiler *compiler = new Compiler(str);
			compiler->startAnalize();

			for (int i = 0; i < compiler->lexer->linesAndErrors.size(); i++) {
				fout << compiler->lexer->linesAndErrors[i] << endl;
			}

			delete compiler;
			compiler = nullptr;

		}
	}


	

	fout.close();
	
	system("pause");
	return 0;
}