#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "CSymbol.h"
#include "defines.h"


using namespace std;



class CLexer
{
public:
	CLexer(string code);
	~CLexer();
	CSymbol* getNextSymbol();

	textPosition positionnow;
private:
	bool isFinish;

	void printSymbolInfo(CSymbol *sym);

	void checkLetterNumber();
	void parseLetters(CSymbol * sym);
	void parseNumbers(CSymbol * sym);
	void setAsIdent(CSymbol * sym, std::string &curResult);
	void makeLowercase();

	void checkOpenBracket(CSymbol * &sym);
	void checkQuotes(CSymbol * sym);
	void checkApostrophes(CSymbol * sym);


	void fillMap();
	char nextChar();

	void PrintLine();
	void PrintErrors();
	bool ReadNextLine();

	char ch;  //������� �����
	short LastInLine; //���������� ����� � ������� ������
	string line; //����� �����-������
	

	CSymbol * nextSym;

private:
	/*������*/
	short ErrSum; //���������� ������������ ������ � ���. ������
	vector <Error> ErrList;
	bool ErrorOverFlow;

	vector <string> lines;
	map <string, int> keywordMap;

public:
	void error(int errorCode, textPosition errorPosition, ErrorType type);
	void skip(int symCode);
	vector <string> linesAndErrors;

};

