#pragma once
#include <iostream>
#include <string>

using namespace std; 
class CIdentifier
{
public:
	enum UseType
	{
		VAR = 0,
		TYPE = 1,
		PROG = 2,
		PROC = 3
	};

	CIdentifier();
	CIdentifier(UseType mTypeofUse, string mIdentName);
	virtual ~CIdentifier();
	int getTypeofUse();
	string getIdentName();
protected:
	UseType mTypeofUse;
	string mIdentName;
};

