/*
Symbol = 1)�������� 2) ������������� 3)���������(����� � �.�.) 4)keyword � �������� �����
��������������
1.	�������� ������� ��� ��������
2.	����������
3.	���(����� ���� ��� ���������, ��� � ��� ����������������)
���������
1.	������� ���(�����, ������������ �����, ������, char, bool)
*/
enum ErrorType
{
	lex,
	synt,
	sem
};
enum SymbolType
{
	stKeyWords,
	stOperator,
	stIdent,
	stConst
};

enum operandType //�������� (�����������)
{
	star = 21,
	slash = 60,
	equal = 16,
	comma = 20,
	semicolon = 14,
	colon = 5,
	point = 61,
	arrow = 62,
	leftpar = 9,
	rightpar = 4,
	lbracket = 11,
	rbracket = 12,
	flpar = 63,
	frpar = 64,
	later = 65,
	greater = 66,
	laterequal = 67,
	greaterequal = 68,
	latergreater = 69,
	plus = 70,
	minus = 71,
	lcomment = 72,
	rcomment = 73,
	assign = 51,
	twopoints = 74
};

enum keywordType //�������� ����� 
{
	ident = 2,
	dosy = 54,
	ifsy = 56,
	insy = 22,
	ofsy = 8,
	orsy = 23,           /* �����������  !   */
	tosy = 55,
	andsy = 24,            /* �����������   &  */
	divsy = 25,
	endsy = 13,
	forsy = 26,
	modsy = 27,
	nilsy = 89,
	notsy = 28,
	setsy = 29,
	varsy = 30,
	casesy = 31,
	elsesy = 32,
	filesy = 57,
	gotosy = 33,
	onlysy = 90,
	thensy = 52,
	typesy = 34,
	unitsy = 35,
	usessy = 36,
	withsy = 37,
	arraysy = 38,
	beginsy = 17,
	constsy = 39,
	labelsy = 40,
	untilsy = 53,
	whilesy = 41,
	downtosy = 55,
	exportsy = 91,
	importsy = 92,
	modulesy = 93,
	packedsy = 42,
	recordsy = 43,
	repeatsy = 44,
	vectorsy = 45,
	stringsy = 46,
	forwardsy = 47,
	processsy = 48,
	programsy = 3,
	segmentsy = 49,
	functionsy = 77,
	separatesy = 78,
	interfacesy = 79,
	proceduresy = 80,
	qualifiedsy = 94,
};

enum ValueType //���������
{
	floatc = 82,
	intc = 15,
	charc = 83,
	stringc = 84,
	booleanc = 85
}; 


