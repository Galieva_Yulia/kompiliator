#include "CLexer.h"
#include "CScope.h"
class Syntaxer
{
public:
	Syntaxer(CLexer *lex);
	~Syntaxer();
	void scanProgram();

private:
	CLexer *lexer = nullptr;

	CSymbol* curSymbol; //����� �������� ��� �������� ������������������ �������� ��������

	CScope* fictScope;
	CScope* curScope;
	void closeCurrentScope();
	int expextedSym;
	bool isSkipping;
	CType* compareTypes(CType * typeOne, CType * typeTwo, int operation);
	bool compareTypes(CType* a, CType* b);
	bool checkType(CType * typeOne, int use, string identName);
	CIdentifier* getIdentifier(CScope* scope, string ident);
	void initializeFictScope();

	bool checkSymbol(int symCode);
	void syntaxError(int symCode);
	void scanBlock();

	void scanTypesSection();
	void scanTypeDefine();
	void scanType();

	void scanVariablesSection();
	void scanOneTypeVariables(int code);

	void scanFuncAndProcSections();
	void scanProcedures();
	void scanProceduresTitle();

	void scanFormalParametersSection();
	void scanParametresGroup(int code);

	void identIdent();

	void scanOperatorsSection();
	void scanCompositeOperator();
	void scanOperator();

	void scanSimpleOperator();

	CType* scanAssignmentOperator();
	CType* scanVariable();
	CType* scanExpression();
	CType* scanSimpleExpression();
	void scanSign();
	CType* scanTerm();
	CType* scanFactor();
	CType* unsignedConstant();
	void multOpp();

	void scanProcedureOperator(string name);
	CType* scanActualParameter();

	void scanZnak();

	void scanHardOperator();
	void scanCycleOperator();
	void scanPostCycleOperator();
	void scanPreCycleOperator();
	void scanParamCycleOperator();
	CIdentifier * NewFunction();
	void scanCondOperator();


	void relationOperation();
	void direction();
	
	void error(int symCode);
	void skip(int symCode);
};

