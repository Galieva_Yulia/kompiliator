#include "CLexer.h"
#include <sstream>
#include <string>
#include <map>
#include <cmath> 
#include "Maps.h"
using namespace std;

CLexer::CLexer(string code)
{
	short ErrSum = 0;
	bool ErrorOverFlow = true;

	positionnow.charNumber = 0;
	positionnow.lineNumber = 0;

	stringstream ss(code);
	string str;
	while (getline(ss, str, '\n')) {
		lines.push_back(str);
	}
	line = lines[0];
	ch = line[0];
	LastInLine = line.size();
	if (line[LastInLine - 1] == '\0')
		line[LastInLine - 1] = ' ';


	isFinish = false;

	fillMap();
}

CLexer::~CLexer()
{
}

void CLexer::fillMap()
{
	keywordMap = {
		{ "do",54 },
	{ "if",56 },
	{ "in",22 },
	{ "of",8 },
	{ "or",23 },
	{ "to",55 },
	{ "and",24 },
	{ "div",25 },
	{ "end",13 },
	{ "for",26 },
	{ "mod",27 },
	{ "nil",89 },
	{ "not",28 },
	{ "set",29 },
	{ "var",30 },
	{ "case",31 },
	{ "else",32 },
	{ "file",57 },
	{ "goto",33 },
	{ "only",90 },
	{ "then",52 },
	{ "type",34 },
	{ "unit",35 },
	{ "uses",36 },
	{ "with",37 },
	{ "array",38 },
	{ "begin",17 },
	{ "const",39 },
	{ "label",40 },
	{ "until",53 },
	{ "while",41 },
	{ "downto",55 },
	{ "export",91 },
	{ "import",92 },
	{ "module",93 },
	{ "packed",42 },
	{ "record",43 },
	{ "repeat",44 },
	{ "vector",45 },
	{ "string",46 },
	{ "forward",47 },
	{ "process",48 },
	{ "program",3 },
	{ "segment",49 },
	{ "function",77 },
	{ "separate",78 },
	{ "interface",79 },
	{ "procedure",80 },
	{ "qualified",94 }

	};


	/*operatorMap =
	{
	{ "star",21 },
	{ "slash",60 },
	{ "equal",16 },
	{ "comma",20 },
	{ "semicolon",14 },
	{ "colon",5 },
	{ "point",61 },
	{ "arrow",62 },
	{ "leftpar",9 },
	{ "rightpar",4 },
	{ "lbracket",11 },
	{ "rbracket",12 },
	{ "flpar",63 },
	{ "frpar",64 },
	{ "later",65 },
	{ "greater",66 },
	{ "laterequal",67 },
	{ "greaterequal",68 },
	{ "latergreater",69 },
	{ "plus",70 },
	{ "minus",71 },
	{ "lcomment",72 },
	{ "rcomment",73 },
	{ "assign",51 },
	{ "twopoints",7 }
	};
	*/

}

//�� ����������� ���������� �� char
char CLexer::nextChar()
{
	if (positionnow.charNumber == LastInLine)
	{
		PrintLine();
		if (ErrSum > 0)
			PrintErrors();
		bool isSuccess = ReadNextLine();
		if (!isSuccess) {
			isFinish = true; //���������� ��� �����
			return ch;
		}
	}
	else
		positionnow.charNumber++;

	ch = line[positionnow.charNumber];
	return ch;
}

CSymbol* CLexer::getNextSymbol()
{
	CSymbol* sym = nullptr;

	if (nextSym != nullptr && nextSym->curSymbolType == SymbolType::stOperator && nextSym->operatorVal == operandType::twopoints) {
		sym = nextSym;

		delete nextSym;
		nextSym = nullptr;

		return sym;
	}

	if (!isFinish)
	{
		sym = new CSymbol();
		while (((ch == ' ') || (ch == '\t') || (ch == '\n') || (ch == '\0')) && (!isFinish)) ch = nextChar();
		if (isFinish)
			return nullptr;

		sym->position.charNumber = positionnow.charNumber;
		sym->position.lineNumber = positionnow.lineNumber;

		char commomCase = ch;	//����� ����� � ����� �������������� ���������
		checkLetterNumber();

		switch (ch)
		{
		case 'a':    //������� ���� �������� �����, ���� �������������
			ch = commomCase; //���������� �������� �������� � ���������� ������� ������
			parseLetters(sym);
			break;

		case '0':
			ch = commomCase;
			parseNumbers(sym);
			break;

		case ',': {
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::comma;
			sym->symbolCode = operandType::comma;
			ch = nextChar();
		}
				  break;

		case '<':
			ch = nextChar();
			if (ch == '=')
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::laterequal;
				sym->symbolCode = operandType::laterequal;
				ch = nextChar();
			}
			else if (ch == '>')
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::latergreater;
				sym->symbolCode = operandType::latergreater;
				ch = nextChar();
			}
			else {
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::later;
				sym->symbolCode = operandType::later;
			}
			break;


		case '>':
			ch = nextChar();
			if (ch == '=')
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::greaterequal;
				sym->symbolCode = operandType::greaterequal;
				ch = nextChar();
			}
			else {
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::greater;
				sym->symbolCode = operandType::greater;
			}
			break;

		case ':':
			ch = nextChar();
			if (ch == '=')
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::assign;
				sym->symbolCode = operandType::assign;
				ch = nextChar();
			}
			else {
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::colon;
				sym->symbolCode = operandType::colon;
			}
			break;

		case '.':
			ch = nextChar();
			if (ch == '.')
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::twopoints;
				sym->symbolCode = operandType::twopoints;
				ch = nextChar();
			}
			else
			{
				sym->curSymbolType = SymbolType::stOperator;
				sym->operatorVal = operandType::point;
				sym->symbolCode = operandType::point;
			}
			break;


		case '(':
			checkOpenBracket(sym);
			break;

		case ')': {
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::rightpar;
			sym->symbolCode = operandType::rightpar;
			ch = nextChar();
		}break;


		case '{':
			while (ch != '}' && !isFinish)
				ch = nextChar();
			if (ch == '}')
			{
				ch = nextChar();
				sym = getNextSymbol();
			}
			else
			{
				//TODO ������� ������, ��� ��� ����� �����
				//cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
				sym->symbolCode = -1;
				error(86, positionnow, ErrorType::lex);
			}

			break;

		case ';':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::semicolon;
			sym->symbolCode = operandType::semicolon;
			ch = nextChar();
			break;

		case '+':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::plus;
			sym->symbolCode = operandType::plus;
			ch = nextChar();
			break;

		case '-':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::minus;
			sym->symbolCode = operandType::minus;
			ch = nextChar();
			break;

		case '*':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::star;
			sym->symbolCode = operandType::star;
			ch = nextChar();
			break;

		case '/':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::slash;
			sym->symbolCode = operandType::slash;
			ch = nextChar();
			break;

		case '=':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::equal;
			sym->symbolCode = operandType::equal;
			ch = nextChar();
			break;

		case '[':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::lbracket;
			sym->symbolCode = operandType::lbracket;
			ch = nextChar();
			break;

		case ']':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::rbracket;
			sym->symbolCode = operandType::rbracket;
			ch = nextChar();
			break;

		case '^':
			sym->curSymbolType = SymbolType::stOperator;
			sym->operatorVal = operandType::arrow;
			sym->symbolCode = operandType::arrow;
			ch = nextChar();
			break;

		case '"': // ��������� string
			checkQuotes(sym);
			break;

		case '\'': //���������� ��������� ����������� � ���������
			checkApostrophes(sym);
			break;

		case '\0':
			ch = nextChar();
			break;

		default: { /*TODO ��������*/}
				 //cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
				 //return nullptr;
				 sym->symbolCode = -1;
				 error(6, positionnow, ErrorType::lex);
				 while (ch != ' ')
					 ch = nextChar();
		}

		/*if (sym->symbolCode != -1)
			printSymbolInfo(sym);*/
		return sym;
	}
	else
	{
		//����� nullptr ������������ ������ � ������ ����� ���� ���������
		return nullptr;
	}
}

void CLexer::printSymbolInfo(CSymbol *sym)
{
	switch (sym->curSymbolType)
	{
	case stKeyWords:
		cout << "keyword " << sym->keywordVal << endl;
		break;
	case stOperator:
		cout << "operator " << sym->operatorVal << endl;
		break;
	case stIdent:
		cout << "ident " << sym->identVal << endl;
		break;
	case stConst:
		cout << "const " << sym->valueVal << endl;
		break;
	}
}

void CLexer::checkOpenBracket(CSymbol * &sym)
{
	char expChar = NULL;
	ch = nextChar();

	if (ch == '*')
		expChar = '*';
	if (ch == '\'')
		expChar = '\'';

	if ((expChar != NULL) && (ch == expChar))
	{

		ch = nextChar();
		while ((ch != expChar) && (!isFinish))
			ch = nextChar();

		if (ch == expChar)
		{
			ch = nextChar();
			if (ch == ')')
			{
				//TODO ���� ������ ������ - �����������, ���� ������ ��� ���������� � ���������� ���� ������
				//while (ch != ' '&& ch != '\0')
				ch = nextChar();
				sym = getNextSymbol();
			}
			else
			{
				//cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
				sym->symbolCode = -1;
				error(4, positionnow, ErrorType::lex); //������ ���� ���� ������
			}
		}
		else
		{
			//TODO ������� ������, ��� ��� ����� �����
			//cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
			sym->symbolCode = -1;
			error(86, positionnow, ErrorType::lex); //������� �� ������
		}
	}
	else
	{
		sym->curSymbolType = SymbolType::stOperator;
		sym->operatorVal = operandType::leftpar;
		sym->symbolCode = operandType::leftpar;
	}
}

void CLexer::checkQuotes(CSymbol * sym)
{
	string curResult;

	ch = nextChar();
	while (ch != '"' && !isFinish && curResult.size() < IDENTLENGHT) {
		curResult.append(1, ch);
		ch = nextChar();
	}

	sym->curSymbolType = SymbolType::stConst;
	sym->valueVal = new CStringValue(curResult);
	sym->symbolCode = ValueType::stringc;
	ch = nextChar();
}

void CLexer::checkApostrophes(CSymbol * sym)
{
	/*�������� �� ������ ch c = '''' - ��� 1 �������� (��� �������� ������!)
	� �� ������� ������ char c = '3' */

	ch = nextChar();
	if (ch == '\'')
	{
		if ((ch = nextChar()) != '\'')
		{
			//TODO ������ ������, �� ��� ������ ''
			//cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
			sym->symbolCode = -1;
			error(6, positionnow, ErrorType::lex);

		}
		else
			if ((ch = nextChar()) == '\'')
			{
				sym->curSymbolType = SymbolType::stConst;
				sym->valueVal = new CCharValue('\'');
				sym->symbolCode = ValueType::charc;
				ch = nextChar();
			}
	}
	else
	{
		//��� ���������� ���������
		char res = ch;
		ch = nextChar();

		if (ch == '\'')
		{
			sym->curSymbolType = SymbolType::stConst;
			sym->valueVal = new CCharValue(res);
			sym->symbolCode = ValueType::charc;
			ch = nextChar();
		}
		else
		{
			//TODO ������ ������ : ������ ������������� ��� string � char
			//cout << "ERROR! Char num: " << positionnow.charNumber << " line num:" << positionnow.lineNumber << endl;
			sym->symbolCode = -1;
			//error 

		}
	}
}

void CLexer::parseLetters(CSymbol * sym)
{
	string curResult; // ������, ���� "����������" ������

	while (((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
		|| (ch >= '0' && ch <= '9') || ch == '_') && curResult.size() < IDENTLENGHT) {

		makeLowercase();

		curResult.append(1, ch);/* ����� ������ � ������ */
		ch = nextChar();/* ��������� ������ */
	}

	if (curResult.size() > KEYWORDLENGHT) { /* ���� ��������� ������������ ����� �������� ����, ������ ��� ������������� ������������ */
		setAsIdent(sym, curResult);
	}
	else
	{
		auto search = keywordMap.find(curResult);
		if (search != keywordMap.end()) {
			sym->curSymbolType = SymbolType::stKeyWords;
			sym->keywordVal = keywordType(search->second); //�������, ��� ������ ����� ��� �����-�� � �������� �����-��
			sym->symbolCode = search->second;
		}
		else
			setAsIdent(sym, curResult);
	}
}

void CLexer::setAsIdent(CSymbol * sym, std::string &curResult)
{
	sym->curSymbolType = SymbolType::stIdent;
	sym->identVal = curResult;
	sym->symbolCode = keywordType::ident;
}

void CLexer::makeLowercase()
{
	if (ch<'a'&& ch != '_' && ch>'9')
		ch += 32;
}

void CLexer::checkLetterNumber()
{
	if (ch >= '0' &&  ch <= '9') ch = '0';
	if ((ch >= 'a' &&  ch <= 'z') || (ch >= 'A' && ch <= 'Z'))  ch = 'a';
	if ((ch >= '�' && ch <= '�') || (ch >= '�' && ch <= '�')) ch = 'c';
}

void CLexer::parseNumbers(CSymbol * sym)
{
	int curResult = 0;
	int integralPart; //����� ����� �����
	int nAfterComma = 0; //���������� ���� ����� �������
	int differ = 0; //��� ����� �� ������� ����� ���� float (��������� ������)

	while (ch >= '0' && ch <= '9')
	{
		integralPart = ch - '0';
		curResult = 10 * curResult + integralPart;
		ch = nextChar();
	}

	if (ch == '.') {
		ch = nextChar();
		if (ch == '.') {
			sym->curSymbolType = SymbolType::stConst;
			sym->valueVal = new CIntValue(curResult);
			sym->symbolCode = ValueType::intc;

			/*
			����� ���������
			CValue* p = sym->valueVal;
			CIntValue *t = dynamic_cast<CIntValue*>(p);*/

			nextSym = new CSymbol();
			nextSym->curSymbolType = SymbolType::stOperator;
			nextSym->operatorVal = operandType::twopoints;
			nextSym->symbolCode = operandType::twopoints;
			ch = nextChar();
		}
		else {
			float val = curResult;
			float tmp = 0;
			//�� �������� ����� �������, �� ������ ������ ����� (�� ����� ���������)
			while (ch >= '0' && ch <= '9') {
				nAfterComma++;

				val += (ch - '0') / pow(10, nAfterComma);
				ch = nextChar();
			}

			//TODO ��������� ������
			sym->curSymbolType = SymbolType::stConst;
			sym->valueVal = new CRealValue(val);
			sym->symbolCode = ValueType::floatc;
			//ch = nextChar();
		}
	}
	else {
		sym->curSymbolType = SymbolType::stConst;
		sym->valueVal = new CIntValue(curResult);
		sym->symbolCode = ValueType::intc;
	}
}

void CLexer::PrintLine()
{
	linesAndErrors.insert(linesAndErrors.end(), std::to_string(positionnow.lineNumber) + ". " + line);
}

void CLexer::PrintErrors()
{
	for (int i = 0; i < ErrList.size(); i++)
	{
		string text, textEr;

		if (ErrorCodeMaps.count(ErrList[i].errorCode) > 0)
			textEr = ErrorCodeMaps[ErrList[i].errorCode];

		switch (ErrList[i].type) {
		case ErrorType::lex:
			text = "���. " + std::to_string(ErrList[i].errorPosition.lineNumber);
			text += ", ���. " + std::to_string(ErrList[i].errorPosition.charNumber);
			text += ": ����������� ������ (" + std::to_string(ErrList[i].errorCode) + ") :" +textEr;
			//text += 
			break;
		case ErrorType::synt:
			text = "���.  " + std::to_string(ErrList[i].errorPosition.lineNumber);
			text += ", ���. " + std::to_string(ErrList[i].errorPosition.charNumber);
			text += ": �������������� ������ (" + std::to_string(ErrList[i].errorCode) +") :"+  textEr;
			break;
		case ErrorType::sem:
			text = "���. " + std::to_string(ErrList[i].errorPosition.lineNumber);
			text += ", ���. " + std::to_string(ErrList[i].errorPosition.charNumber);
			text += ". ������������� ������ (" + std::to_string(ErrList[i].errorCode)+ ") :" + textEr;
			break;
		}



		linesAndErrors.insert(linesAndErrors.end(), "		" + text);
	}
}

bool CLexer::ReadNextLine()
{
	if (positionnow.lineNumber < lines.size() - 1)
	{
		do {
			line = lines[++positionnow.lineNumber];
			positionnow.charNumber = 0;
			LastInLine = line.size(); //���������� ������� �������� � ������� ������
			ErrList.clear();
		} while (LastInLine == 0);

		if (line[LastInLine - 1] == '\n' || line[LastInLine - 1] == '\0')
			line[LastInLine - 1] = ' ';

		return true; //���������� ���� ������� ���� ������
	}
	else
	{
		//printf("end of file");
		return false; //���������� ���� �������� ����� �����
	}
}


void CLexer::error(int errorCode, textPosition errorPosition, ErrorType type)
{
	//if (type == ErrorType::sem)
	//	cout << "SEMANTIC ERROR!!!!!!!!  Line    " <<positionnow.lineNumber<< "  char   "<<positionnow.charNumber<< endl;
	if (ErrSum == ERRMAX)
		ErrorOverFlow = true;
	else {
		ErrSum++;
		Error er;
		er.errorCode = errorCode;
		er.errorPosition = errorPosition;
		er.type = type;
		ErrList.insert(ErrList.end(), er);
	}
}

void CLexer::skip(int symCode)
{
	CSymbol *sym = new CSymbol();
	do
	{
		sym = getNextSymbol();
	} while (!isFinish && sym != nullptr && sym->symbolCode != symCode);
}




